import pygame
from sys import exit
from random import randint

pygame.init()
screen = pygame.display.set_mode((1000,600))
pygame.display.set_caption("Projekt Hra")
clock = pygame.time.Clock()
test_font = pygame.font.Font("font/Pixeltype.ttf",50)
game_active=False

move_right=False
move_left=False
move_up=False
move_down=False

#pozadie na start hry
start_surface = pygame.Surface((1000,600))
start_surface.fill("lightblue")
title_text = test_font.render("Random nazov este vymyslime", False, (64, 64, 64))
title_text_rectangle = title_text.get_rect(center = (500,50))
start_picture = pygame.image.load("game/misa.PNG").convert_alpha()


level1_surface = pygame.image.load("game/1_level_svetly.png").convert_alpha()

#postava
player_surface = pygame.image.load("game/character.png").convert_alpha()
player_rectangle = player_surface.get_rect(center=(80,300))
player_hit=False

#enemy
enemy_surface = pygame.image.load("game/enemy.png").convert_alpha()
enemy_rectangle = enemy_surface.get_rect(center=(500,200))
enemy_hit=False


#strielanie, mozem mat vzdy len jeden naboj vystreleny
bullet_surface = pygame.image.load("game/bullet.png").convert_alpha()
bullet_rectangle = bullet_surface.get_rect(center=(0,0))
bullet_state = "ready" #naboj doletel a je pripraveny dalsi

def pohyb_enemy():

    if enemy_rectangle.x<1000:
        enemy_rectangle.x+=10
        #print(enemy_rectangle.x)
    if enemy_rectangle.x==1000:
        enemy_rectangle.x=20


#hlavne telo
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()


        if game_active==True:
            #pohyb
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    move_right = True
                if event.key == pygame.K_LEFT:
                    move_left = True
                if event.key == pygame.K_UP:
                    move_up = True
                if event.key == pygame.K_DOWN:
                    move_down = True
                if event.key == pygame.K_DOWN:
                    move_down = True
                #vystrel
                if event.key == pygame.K_SPACE:
                    if bullet_state == "ready":
                        bullet_state = "fire"
                        bullet_rectangle.x = player_rectangle.x + 8
                        bullet_rectangle.y = player_rectangle.y - 20

            #pohyb
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    move_right = False
                if event.key == pygame.K_LEFT:
                    move_left = False
                if event.key == pygame.K_UP:
                    move_up = False
                if event.key == pygame.K_DOWN:
                    move_down = False
        else:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    game_active=True
                    player_hit=False
                    enemy_rectangle.x=500
                    enemy_rectangle.y=200



    #ak je hodnota true tak to znamena ze drzim klavesu a pohubujem sa bez toho aby som ju musel znovu stlacit2
    if move_right == True and player_rectangle.x<943:
        player_rectangle.x+=7
    if move_left  == True and player_rectangle.x>7:
        player_rectangle.x-=7
    if move_up == True and player_rectangle.y>90:
        player_rectangle.y-=7
    if move_down == True and player_rectangle.y<520:
        player_rectangle.y+=7

    if game_active == True:
        screen.blit(level1_surface,(0,0))
        screen.blit(player_surface,player_rectangle)

        #hra sa skoci a resetuje sa hodnota
        if player_hit==True:
            game_active = False
            player_rectangle.x=80
            player_rectangle.y=300

        #ak sa hrac dotkne enemy tak sa hodnota hit da true=zomrel si, zatial iba na 1 zivot, v buducnosti sa rozsiri
        if player_rectangle.colliderect(enemy_rectangle):
            player_hit=True

        #ak enemy zije
        if enemy_hit == False:
            screen.blit(enemy_surface, enemy_rectangle)
            pohyb_enemy()

        #naboj narazi do enemy, prepne sa na true, a jeho hitbox sa odjebe dakam do daleka aby nerusil
        if bullet_rectangle.colliderect(enemy_rectangle):
            #enemy_hit = True
            enemy_rectangle.x=-1000 #ked enemy zomrie tak ho spawnem za obrazovku do lava, vieme nastavit random spawn interval
            print(enemy_rectangle.x)

            print("hit")
            bullet_rectangle.y = -50 #naboj dam niekam nabok lebo ptm dostaval enemy hit na mieste kde som ho trafil minule


            # ak som stlacil space tak sa vystreli
        if bullet_state == "fire":
            screen.blit(bullet_surface, (bullet_rectangle.x, bullet_rectangle.y))
            bullet_rectangle.y -= 10
            print(bullet_rectangle.y)
            if bullet_rectangle.y < 90 or bullet_rectangle.colliderect(enemy_rectangle):
                bullet_state = "ready"



    else:
        screen.blit(start_surface,(0,0))
        screen.blit(title_text, title_text_rectangle)
        screen.blit(start_picture, (250, 85))


    pygame.display.update()
    clock.tick(60)
